<?php

/**
 * @file
 * uw_ct_exchange_program_overridden.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_exchange_program_overridden_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: context.
  $overrides["context.exchange_university_home_page.conditions|path|values|node/13"] = 'node/13';
  $overrides["context.exchange_university_home_page.conditions|path|values|outgoing-exchange-students/exchange-programs"]["DELETED"] = TRUE;
  $overrides["context.exchange_university_home_page.reactions|block|blocks|nodeblock-118"] = array(
    'module' => 'nodeblock',
    'delta' => 118,
    'region' => 'content',
    'weight' => 34,
  );
  $overrides["context.exchange_university_home_page.reactions|block|blocks|nodeblock-119"] = array(
    'module' => 'nodeblock',
    'delta' => 119,
    'region' => 'content',
    'weight' => 36,
  );
  $overrides["context.exchange_university_home_page.reactions|block|blocks|nodeblock-48"]["DELETED"] = TRUE;
  $overrides["context.exchange_university_home_page.reactions|block|blocks|nodeblock-49"]["DELETED"] = TRUE;
  $overrides["context.exchange_university_home_page.reactions|block|blocks|views-7e0cee13cf3d450d155cdd38e1600549"]["DELETED"] = TRUE;
  $overrides["context.exchange_university_profile_under_country.reactions|block|blocks|views-f7578fed8795cd4254691ba6915a57bb"]["DELETED"] = TRUE;
  $overrides["context.exchange_university_profile_under_program.reactions|block|blocks|views-7e7ddd7c7f43205509184608c763291b"]["DELETED"] = TRUE;

  // Exported overrides for: variable.
  $overrides["variable.pathauto_node_uw_exchange_contact_pattern.value"] = 'go-abroad/exchange-programs/contact/[node:title]';
  $overrides["variable.pathauto_node_uw_exchange_program_block_pattern.value"] = 'go-abroad/exchange-programs/[node:title]';
  $overrides["variable.pathauto_node_uw_exchange_student_pattern.value"] = 'go-abroad/exchange-programs/profile/[node:title]';
  $overrides["variable.pathauto_node_uw_exchange_university_pattern.value"] = 'go-abroad/exchange-programs/university/[node:title]';

  // Exported overrides for: views_view.
  $overrides["views_view.exchange_by_faculty_block.display|default|display_options|header|area|content"] = '<a href="/international/go-abroad/exchange-programs/all-faculties">All Faculties</a>';
  $overrides["views_view.exchange_university_by_faculty_page.display|all_exchange_universities_page|display_options|path"] = 'go-abroad/exchange-programs/all-faculties';
  $overrides["views_view.exchange_university_by_faculty_page.display|exchange_university_faculty_page|display_options|path"] = 'go-abroad/exchange-programs/faculty';

  return $overrides;
}
