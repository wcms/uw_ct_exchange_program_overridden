<?php

/**
 * @file
 * uw_ct_exchange_program_overridden.features.inc
 */

/**
 * Implements hook_context_default_contexts_alter().
 */
function uw_ct_exchange_program_overridden_context_default_contexts_alter(&$data) {
  if (isset($data['exchange_university_home_page'])) {
    $data['exchange_university_home_page']->conditions['path']['values']['node/13'] = 'node/13'; /* WAS: '' */
    $data['exchange_university_home_page']->reactions['block']['blocks']['nodeblock-118'] = array(
      'module' => 'nodeblock',
      'delta' => 118,
      'region' => 'content',
      'weight' => 34,
    ); /* WAS: '' */
    $data['exchange_university_home_page']->reactions['block']['blocks']['nodeblock-119'] = array(
      'module' => 'nodeblock',
      'delta' => 119,
      'region' => 'content',
      'weight' => 36,
    ); /* WAS: '' */
    unset($data['exchange_university_home_page']->conditions['path']['values']['outgoing-exchange-students/exchange-programs']);
    unset($data['exchange_university_home_page']->reactions['block']['blocks']['nodeblock-48']);
    unset($data['exchange_university_home_page']->reactions['block']['blocks']['nodeblock-49']);
    unset($data['exchange_university_home_page']->reactions['block']['blocks']['views-7e0cee13cf3d450d155cdd38e1600549']);
  }
  if (isset($data['exchange_university_profile_under_country'])) {
    unset($data['exchange_university_profile_under_country']->reactions['block']['blocks']['views-f7578fed8795cd4254691ba6915a57bb']);
  }
  if (isset($data['exchange_university_profile_under_program'])) {
    unset($data['exchange_university_profile_under_program']->reactions['block']['blocks']['views-7e7ddd7c7f43205509184608c763291b']);
  }
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_exchange_program_overridden_strongarm_alter(&$data) {
  if (isset($data['pathauto_node_uw_exchange_contact_pattern'])) {
    $data['pathauto_node_uw_exchange_contact_pattern']->value = 'go-abroad/exchange-programs/contact/[node:title]'; /* WAS: 'outgoing-exchange-students/exchange-programs/contact/[node:title]' */
  }
  if (isset($data['pathauto_node_uw_exchange_program_block_pattern'])) {
    $data['pathauto_node_uw_exchange_program_block_pattern']->value = 'go-abroad/exchange-programs/[node:title]'; /* WAS: 'outgoing-exchange-students/exchange-programs/[node:title]' */
  }
  if (isset($data['pathauto_node_uw_exchange_student_pattern'])) {
    $data['pathauto_node_uw_exchange_student_pattern']->value = 'go-abroad/exchange-programs/profile/[node:title]'; /* WAS: 'outgoing-exchange-students/exchange-programs/profile/[node:title]' */
  }
  if (isset($data['pathauto_node_uw_exchange_university_pattern'])) {
    $data['pathauto_node_uw_exchange_university_pattern']->value = 'go-abroad/exchange-programs/university/[node:title]'; /* WAS: 'outgoing-exchange-students/exchange-programs/university/[node:title]' */
  }
}

/**
 * Implements hook_views_default_views_alter().
 */
function uw_ct_exchange_program_overridden_views_default_views_alter(&$data) {
  if (isset($data['exchange_by_faculty_block'])) {
    $data['exchange_by_faculty_block']->display['default']->display_options['header']['area']['content'] = '<a href="/international/go-abroad/exchange-programs/all-faculties">All Faculties</a>'; /* WAS: '<a href="/study-abroad-programs/outgoing-exchange-students/exchange-programs/all-faculties">All Faculties</a>' */
  }
  if (isset($data['exchange_university_by_faculty_page'])) {
    $data['exchange_university_by_faculty_page']->display['all_exchange_universities_page']->display_options['path'] = 'go-abroad/exchange-programs/all-faculties'; /* WAS: 'outgoing-exchange-students/exchange-programs/all-faculties' */
    $data['exchange_university_by_faculty_page']->display['exchange_university_faculty_page']->display_options['path'] = 'go-abroad/exchange-programs/faculty'; /* WAS: 'outgoing-exchange-students/exchange-programs/faculty' */
  }
}
